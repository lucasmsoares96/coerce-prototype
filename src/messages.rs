use std::{collections::HashMap, net::SocketAddr};

use actix::prelude::*;
use serde::{Deserialize, Serialize};

pub struct ActionLocation {
    worker: SocketAddr,
    index: usize,
}

#[derive(Serialize, Deserialize)]
pub struct WorkerConfig(pub HashMap<SocketAddr, WorkerAtributes>);
#[derive(Serialize, Deserialize)]
pub struct RegisterRequest;
#[derive(Serialize, Deserialize)]
pub struct RegisterResponse {
    pub socket_addr: SocketAddr,
    pub worker_config: WorkerAtributes,
}

#[derive(Message, Serialize, Deserialize, Clone)]
#[rtype(result = "()")]
pub struct WorkerAtributes {
    // TODO: não sei como fazer isso
    // pub remote_addr: RemoteAddr,
    pub cores_amount: usize,
    pub operations: Vec<Action>,
}

/// Enum that define the operations
#[derive(Message, Eq, Hash, PartialEq, Clone, Debug, Serialize, Deserialize)]
#[rtype(result = "()")]
pub enum Action {
    Source,
    Sink,
    Map(usize),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum OperatorAddr {
    // Remote(RemoteAddr), TODO: descobrir é o remote Addr no coerce
    Local,
}
/// Pair of Action and list of addresses of the Next operators
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ConfigAction {
    pub action: Action,
    pub next: Vec<OperatorAddr>,
}

/// Message to configure the Worker and the Operators
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Config(
    pub Vec<ConfigAction>, // Each index is a Operator Actor
);
