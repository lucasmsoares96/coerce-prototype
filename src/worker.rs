pub mod actors;
mod messages;
use crate::actors::worker::*;
use actors::manager::{Manager, Register};
use coerce::actor::system::ActorSystem;
use coerce::actor::{IntoActor, ToActorId};
use coerce::remote::system::RemoteActorSystem;
use opentelemetry::global;
use opentelemetry::sdk::propagation::TraceContextPropagator;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;

#[macro_use]
extern crate serde;

#[macro_use]
extern crate async_trait;

#[tokio::main]
pub async fn main() {
    global::set_text_map_propagator(TraceContextPropagator::new());

    let (tracer, _uninstall) = opentelemetry_jaeger::new_pipeline()
        .with_service_name("example-worker")
        .install()
        .unwrap();

    let opentelemetry = tracing_opentelemetry::layer().with_tracer(tracer);
    tracing_subscriber::registry()
        .with(opentelemetry)
        .try_init()
        .unwrap();

    let system = ActorSystem::new();
    let remote = RemoteActorSystem::builder()
        .with_tag("example-worker")
        .with_actor_system(system)
        .with_handlers(|handlers| handlers.with_handler::<Manager, Register>("EchoActor.Echo"))
        .build()
        .await;

    remote
        .clone()
        .cluster_worker()
        .listen_addr("localhost:30101")
        .with_seed_addr("localhost:30100")
        .start()
        .await;

    {
        // let span = tracing::info_span!("CreateAndSend");
        // let _enter = span.enter();

        let remote_actor = remote
            .actor_ref::<Manager>("echo-actor".to_actor_id())
            .await
            .expect("unable to get echo actor");

        let local_actor = Worker
            .into_actor(Some("worker".to_string()), remote.actor_system())
            .await
            .expect("Unable to start Worker Actor");

        // let remote_worker_ref: RemoteActorRef<Worker> =
        //     RemoteActorRef::new(remote_actor.actor_id().clone(), remote.node_id(), remote);

        let _result = remote_actor
            .send(Register {
                id: local_actor.id.to_actor_id(),
                node_id: remote.node_id(),
            })
            .await;
        // assert_eq!(result.unwrap(), "hello".to_string());
    }

    tokio::signal::ctrl_c()
        .await
        .expect("failed to listen for event");
}
