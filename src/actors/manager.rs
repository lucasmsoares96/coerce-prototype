use crate::messages::*;
use coerce::actor::context::ActorContext;
use coerce::actor::message::Handler;
use coerce::actor::{Actor, ActorId, ActorRef, IntoActorId};
use coerce::remote::system::NodeId;
use coerce::remote::RemoteActorRef;
use coerce_macros::JsonMessage;
use std::collections::HashMap;
use tokio::net::unix::SocketAddr;

use super::worker::Worker;

pub struct Manager {
    /// Device assiciated to Operation
    pub workers: HashMap<SocketAddr, WorkerAtributes>,
    /// Operation associated to Device
    pub operation: HashMap<Action, ActionLocation>,
    /// Lenght sum of all queues
    pub rank: HashMap<Action, u32>,
    /// Number of cores for all workers
    pub total_cores: usize,
    /// Amount of tasks define in pipeline
    pub total_tasks: usize,
}

impl Manager {
    pub fn new() -> Self {
        Self {
            workers: HashMap::new(),
            operation: HashMap::new(),
            rank: HashMap::new(),
            total_cores: 0,
            total_tasks: 0,
        }
    }
}

impl Actor for Manager {}

#[derive(Debug, Clone, JsonMessage, Serialize, Deserialize)]
#[result("()")]
pub struct Register {
    pub id: ActorId,
    pub node_id: NodeId,
}

#[async_trait]
impl Handler<Register> for Manager {
    async fn handle(&mut self, message: Register, ctx: &mut ActorContext) {
        println!("{:?}", message);
        let actor = self.get_actor(ctx, message.clone()).await;
        let _ = actor
            .send(Register {
                id: ctx.id().into_actor_id(),
                node_id: ctx.system().remote().node_id(),
            })
            .await;
    }
}

impl Manager {
    pub async fn get_actor(&self, ctx: &ActorContext, message: Register) -> ActorRef<Worker> {
        let actor_id = format!("{}", &message.id).into_actor_id();
        let remote = ctx.system().remote();
        let leader = remote.current_leader();
        if leader == Some(remote.node_id()) {
            ctx.system()
                .get_tracked_actor::<Worker>(actor_id)
                .await
                .expect("get local coordinator")
                .into()
        } else {
            RemoteActorRef::<Worker>::new(actor_id, message.node_id, remote.clone()).into()
        }
    }
}
