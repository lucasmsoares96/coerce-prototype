use coerce::actor::context::ActorContext;
use coerce::actor::message::Handler;
use coerce::actor::Actor;
use coerce_macros::JsonMessage;

use super::manager::Register;

#[derive(Serialize, Deserialize)]
pub struct Worker;

impl Actor for Worker {}

#[derive(JsonMessage, Serialize, Deserialize)]
#[result("()")]
pub struct Echo(pub String);

#[async_trait]
impl Handler<Register> for Worker {
    async fn handle(&mut self, message: Register, _ctx: &mut ActorContext) {
        println!("{:?}", message);
    }
}
